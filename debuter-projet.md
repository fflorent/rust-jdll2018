# Démarrer en 5 étapes


## 1. Télécharger rustup

 - Rustup vous facilite l'installation des outils de rust, dont :
   - le compilateur `rustc`
   - le gestionnaire de paquets `cargo`
   - la bibliothèque standard
   - maintenir différentes versions

```bash
curl https://sh.rustup.rs -sSf | sh
```

→ http://rustup.rs/


## 2. Initier votre projet

- . Pour une bibliothèque seule :
```bash
cargo init mon_projet
```

- . Pour un binaire :
```bash
cargo init --bin mon_projet
```


## 3. Hacker

 - . Les sources sont dans le répertoire `src/`
 - . Les meta-informations, dépendances, … sont à mettre dans `Cargo.toml`


## 3. Hacker
```toml
[package]
name = "spotifyd"
version = "0.1.1"
authors = ["Simon Persson <simon@flaskpost.org>"]

[dependencies]
simplelog = "0.4"
xdg = "2.1"
ctrlc = { version = "3.1", features = ["termination"] }
...
```


## 4. Exécuter le projet

- Depuis la racine du projet :
```bash
$ cargo run
```


## 5. Bonus : Écrire les tests unitaires

- à la fin du module :

```rust
#[test]
fn it_sould_work() {
  assert_eq!(true, true, "should pass");
}
```

- Exécuter les tests :

```bash
$ cargo test
```


## Plus simple et rapide

Sandbox en ligne :

→ https://play.rust-lang.org/
